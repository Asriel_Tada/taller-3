from django.shortcuts import render, redirect, get_object_or_404
from .forms import *
from basket.models import (
    Team,
    Player,
    Coach,
    Match
)

def players_list(request):
    template_name = 'players_list.html'
    data = {}

    teams = Team.objects.all()

    for team in teams:
        team.players = Player.objects.filter(team=team)

    data['teams'] = teams

    return render(request, template_name, data)

def teams_list(request):
    template_name = 'teams_list.html'
    data = {}

    data['teams'] = Team.objects.all()
    return render(request, template_name, data)

def coachs_list(request):
    template_name = 'coach_list.html'
    data = {}
    data['coachs'] = Coach.objects.all()

    return render(request, template_name, data)


def matchs_list(request):
    template_name = 'matchs_list.html'
    data = {}

    data['matchs'] = Match.objects.all()

    return render(request, template_name, data)

def home(request):
    template_name = 'home.html'
    data = {}

    data['last_5_matchs'] = Match.objects.all().order_by('-pk')[:5]
    data['last_5_players'] = Player.objects.all().order_by('-pk')[:5]
    data['teams'] = Team.objects.all()

    return render(request, template_name, data)


def indexCoach(request):
    template = 'Coach/list_coach.html'
    coachs = Coach.objects.all()
    context = {"Coach": coachs}
    return render(request, template, context)

def Coach_create(request):
    template = 'Coach/form.html'
    form = CoachForm(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect('Game:indexCoach')
    context = {"form": form}
    return render(request, template, context)


def Coach_update(request, pk):
    template = 'Coach/form.html'
    Coachs = get_object_or_404(Coach, pk=pk)
    form = CoachForm(request.POST or None, instance=Coachs)
    if form.is_valid():
        form.save()
        return redirect('Game:indexCoach')
    context = {"form": form}
    return render(request, template, context)

def Coach_delete(request, pk):
    template = 'Coach/delete.html'
    Coachs = get_object_or_404(Coach, pk=pk)
    if request.method == 'POST':
        Coachs.delete()
        return redirect('Game:indexCoach')
    context = {"Coach": Coach}
    return render(request, template, context)

def indexTeam(request):
    template_name = 'teams_list.html'
    data = {}

    data['teams'] = Team.objects.all()
    return render(request, template_name, data)


def Team_create(request):
    template = 'Team/form.html'
    data = {}
    form = TeamForm(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect('Game:indexTeam')
    context = {"form": form}
    return render(request, template, context)


def Team_update(request, pk):
    template = 'Team/form.html'
    teams = get_object_or_404(Team, pk=pk)
    form = TeamForm(request.POST or None, instance=teams)
    if form.is_valid():
        form.save()
        return redirect('Game:indexTeam')
    context = {"form": form}
    return render(request, template, context)

def Team_delete(request, pk):
    template = 'Team/delete.html'
    Teams = get_object_or_404(Team, pk=pk)
    if request.method == 'POST':
        Teams.delete()
        return redirect('Game:indexTeam')
    context = {"Team": Teams}
    return render(request, template, context)


def indexPlayer(request):
    template = 'Player/Player_list.html'
    Players = Player.objects.all()
    context = {"Player": Players}
    return render(request, template, context)


def Player_create(request):
    template = 'Player/form.html'
    form = PlayerForm(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect('Game:indexPlayer')
    context = {"form": form}
    return render(request, template, context)


def Player_update(request, pk):
    template = 'Player/form.html'
    Players = get_object_or_404(Player, pk=pk)
    form = PlayerForm(request.POST or None, instance=Players)
    if form.is_valid():
        form.save()
        return redirect('Game:indexPlayer')
    context = {"form": form}
    return render(request, template, context)

def Player_delete(request, pk):
    template = 'Player/delete.html'
    Players = get_object_or_404(Player, pk=pk)
    if request.method == 'POST':
        Players.delete()
        return redirect('Game:indexPlayer')
    context = {"Player": Players}
    return render(request, template, context)