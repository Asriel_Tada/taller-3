from django.urls import path, include
from basket import views
app_name = 'Game'

urlpatterns = [
    path('players/list', views.players_list, name="players_list"),   
    path('teams/list', views.teams_list, name="teams_list"),   
    path('coachs/list', views.coachs_list, name="coachs_list"),   
    path('matchs/list', views.matchs_list, name="matchs_list"),

    path('Coach/a', views.indexCoach, name='indexCoach'),
    path('Coach/add/', views.Coach_create, name='Coach_add'),
    path('Coach/update/<int:pk>', views.Coach_update, name='Coach_update'),
    path('Coach/delete/<int:pk>', views.Coach_delete, name='Coach_delete'),

    path('Team/a', views.indexTeam, name='indexTeam'),
    path('Team/add/', views.Team_create, name='Team_create'),
    path('Team/update/<int:pk>', views.Team_update, name='Team_update'),
    path('Team/delete/<int:pk>', views.Team_delete, name='Team_delete'),

    path('Player/a', views.indexPlayer, name='indexPlayer'),
    path('Player/add/', views.Player_create, name='Player_add'),
    path('Player/update/<int:pk>', views.Player_update, name='Player_update'),
    path('Player/delete/<int:pk>', views.Player_delete, name='Player_delete'),
    path('', views.home, name="home"),   
]
